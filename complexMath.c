#include <stdio.h>
#include <stdlib.h>

#include "complex.h"

// Initialize Complex vars
Complex x = {0}, y = {0};

int main(int cnt, char *argv[]) {
    /*
     * Check argument count
     */
    if (cnt != 3) {
        puts("Usage: complexMath re+im[ij] re+im[ij]\n");
        return EXIT_FAILURE;
    }

    /*
     * Read in complex numbers
     */
    char ij[2];
    if (sscanf(argv[1], "%lf%lf%1[ij]", &x.re, &x.im, ij) != 3) {
        printf("\"%s\" is not a valid complex number.", argv[1]);
        return EXIT_FAILURE;
    }

    if (sscanf(argv[2], "%lf%lf%1[ij]", &y.re, &y.im, ij) != 3) {
        printf("\"%s\" is not a valid complex number.", argv[2]);
        return EXIT_FAILURE;
    }

    /*
     * Print x
     */
    printf("%13s%1s", "x", "=");
    printComplex(x);

    /*
     * Print y
     */
    printf("%13s%1s", "y", "=");
    printComplex(y);

    /*
     * Sum
     */
    printf("%13s%1s", "Sum", "=");
    printComplex(add(x, y));

    /*
     * Difference
     */
    printf("%13s%1s", "Difference", "=");
    printComplex(subtract(x, y));

    /*
     * Product
     */
    printf("%13s%1s", "Product", "=");
    printComplex(multiply(x, y));

    /*
     * Quotient
     */
    printf("%13s%1s", "Quotient", "=");
    printComplex(divide(x, y));

    /*
     * Conjugates
     */
    printf("%13s%1s", "x Conjugate", "=");
    printComplex(conjugate(x));

    printf("%13s%1s", "y Conjugate", "=");
    printComplex(conjugate(y));

    /*
     * Magnitudes and Angles
     */
    printf("%13s%1s%9.4lf", "x: Magnitude", "=", magnitude(x));
    printf(", Angle = %9.4lf rads\n", angle(x));

    printf("%13s%1s%9.4lf", "y: Magnitude", "=", magnitude(y));
    printf(", Angle = %9.4lf rads\n", angle(y));


    return EXIT_SUCCESS;
}
