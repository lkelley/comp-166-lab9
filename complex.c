//
// Created by Landon on 1/21/2020.
//

#include <stdio.h>
#include <math.h>

#include "complex.h"

Complex add(const Complex a, const Complex b) {
    Complex result;

    result.im = a.im + b.im;
    result.re = a.re + b.re;

    return result;
}

Complex subtract(const Complex a, const Complex b) {
    Complex result;

    result.im = a.im - b.im;
    result.re = a.re - b.re;

    return result;
}

Complex multiply(const Complex a, const Complex b) {
    Complex result;

    result.im = a.re * b.im + a.im * b.re;
    result.re = a.re * b.re - a.im * b.im;

    return result;
}

Complex divide(const Complex a, const Complex b) {
    Complex result;

    result.re = (a.re * b.re + a.im * b.im) / (pow(b.re, 2) + pow(b.im, 2));
    result.im = (a.im * b.re - a.re * b.im) / (pow(b.re, 2) + pow(b.im, 2));

    return result;
}

Complex conjugate(const Complex a) {
    Complex result;

    result.re = a.re;
    result.im = -a.im;

    return result;
}

double magnitude(const Complex a) {
    return sqrt(pow(a.re, 2) + pow(a.im, 2));
}

double angle(const Complex a) {
    return atan2(a.im, a.re);
}

void printComplex(const Complex a) {
    printf("%9.4lf%+9.4lfi\n", a.re, a.im);
}
