//
// Created by Landon on 1/21/2020.
//

#ifndef LAB_9___COMPLEX_NUMBERS_COMPLEX_H
#define LAB_9___COMPLEX_NUMBERS_COMPLEX_H

/* Struct to represent a complex number */
typedef struct {
    double re, im;
} Complex;

/* Complex arithmetic operators */
Complex add(const Complex a, const Complex b);

Complex subtract(const Complex a, const Complex b);

Complex multiply(const Complex a, const Complex b);

Complex divide(const Complex a, const Complex b);

Complex conjugate(const Complex a);

double magnitude(const Complex a);

/* Returned angle must be in the correct quadrant! */
double angle(const Complex a);

void printComplex(const Complex a);

#endif //LAB_9___COMPLEX_NUMBERS_COMPLEX_H
